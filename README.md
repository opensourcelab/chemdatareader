# ChemDataReader

ChemDataReader is a flexible python framework for reading and converting chemical data originating from webservers, including metadata and semantics.

## Features

## Installation

    pip install chemdatareader --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    chemdatareader --help 

## Development

    git clone gitlab.com/opensourcelab/chemdatareader

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/chemdatareader](https://opensourcelab.gitlab.io/chemdatareader) or [chemdatareader.gitlab.io](chemdatareader.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



